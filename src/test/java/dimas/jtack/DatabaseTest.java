package dimas.jtack;

import org.junit.Test;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

public class DatabaseTest {

  @Test
  public void getPresent() {

    // Given
    Database db = new Database(Collections.singletonMap("a", "10"));

    // When
    String value = db.get("a");

    // Then
    assertThat(value, is("10"));
  }

  @Test
  public void getAbsent() {

    // Given
    Database db = new Database(Collections.singletonMap("a", "10"));

    // When
    String value = db.get("b");

    // Then
    assertThat(value, is(nullValue(String.class)));
  }

  @Test
  public void numEqualToAbsent() {

    // Given
    Database db = new Database();

    // When
    int result = db.numEqualTo("10");

    // Then
    assertThat(result, is(0));
  }

  @Test
  public void numEqualToPresent() {

    // Given
    Database db = new Database(Collections.singletonMap("a", "10"));

    // When
    int result = db.numEqualTo("10");

    // Then
    assertThat(result, is(1));
  }

  @Test
  public void numEqualToMulti() {

    // Given
    Map<String, String> entries = new HashMap<>();
    entries.put("a", "10");
    entries.put("b", "10");
    entries.put("c", "20");
    entries.put("d", "10");

    Database db = new Database(entries);

    // When
    int result = db.numEqualTo("10");

    // Then
    assertThat(result, is(3));
  }

  @Test
  public void setInsert() {

    // Given
    Database db = new Database();

    // When
    db.set("a", "10");

    // Then
    assertThat(db.get("a"), is("10"));
  }

  @Test
  public void setAdd() {

    // Given
    Database db = new Database();

    // When
    db.set("a", "10");

    // Then
    assertThat(db.numEqualTo("10"), is(1));
  }

  @Test
  public void setIncrement() {

    // Given
    Map<String, String> entries = new HashMap<>();
    entries.put("a", "10");
    entries.put("b", "10");
    entries.put("c", "20");
    entries.put("d", "10");

    Database db = new Database(entries);

    // When
    db.set("e", "10");

    // Then
    assertThat(db.numEqualTo("10"), is(4));
  }

  @Test
  public void setUpdate() {

    // Given
    Database db = new Database(Collections.singletonMap("a", "10"));

    // When
    db.set("a", "20");

    // Then
    assertThat(db.get("a"), is("20"));
  }

  @Test
  public void setReplace() {

    // Given
    Database db = new Database(Collections.singletonMap("a", "10"));

    // When
    db.set("a", "20");

    // Then
    assertThat(db.numEqualTo("10"), is(0));
    assertThat(db.numEqualTo("20"), is(1));
  }

  @Test
  public void setReplaceMulti() {

    // Given
    Map<String, String> entries = new HashMap<>();
    entries.put("a", "10");
    entries.put("b", "10");
    entries.put("c", "20");
    entries.put("d", "10");

    Database db = new Database(entries);

    // When
    db.set("b", "20");

    // Then
    assertThat(db.numEqualTo("10"), is(2));
    assertThat(db.numEqualTo("20"), is(2));
  }

  @Test
  public void unsetPresent() {

    // Given
    Database db = new Database(Collections.singletonMap("a", "10"));

    // When
    db.unset("a");

    // Then
    assertThat(db.get("a"), is(nullValue(String.class)));
  }

  @Test
  public void unsetDecrement() {

    // Given
    Database db = new Database(Collections.singletonMap("a", "10"));

    // When
    db.unset("a");

    // Then
    assertThat(db.numEqualTo("10"), is(0));
  }

  @Test
  public void unsetDecrementMulti() {

    // Given
    Map<String, String> entries = new HashMap<>();
    entries.put("a", "10");
    entries.put("b", "10");
    entries.put("c", "20");
    entries.put("d", "10");

    Database db = new Database(entries);

    // When
    db.unset("a");

    // Then
    assertThat(db.numEqualTo("10"), is(2));
    assertThat(db.numEqualTo("20"), is(1));
  }

  @Test
  public void unsetAbsent() {

    // Given
    Database db = new Database();

    // When
    db.unset("a");

    // Then
    assertThat(db.get("a"), is(nullValue(String.class)));
  }

  @Test
  public void hasNoTransaction() {

    // Given
    Database db = new Database();

    // When
    boolean result = db.isInTransaction();

    // Then
    assertThat(result, is(false));
  }

  @Test
  public void beginEntersTransaction() {

    // Given
    Database db = new Database();

    // When
    db.begin();

    // Then
    assertThat(db.isInTransaction(), is(true));
  }

  @Test
  public void beginEntersNestedTransaction() {

    // Given
    Database db = new Database();
    db.begin();

    // When
    db.begin();

    // Then
    assertThat(db.isInTransaction(), is(true));
  }

  @Test(expected = IllegalStateException.class)
  public void commitNoTransaction() {
    Database db = new Database();
    db.commit();
  }

  @Test
  public void commitClosesTransaction() {

    // Given
    Database db = new Database();
    db.begin();

    // When
    db.commit();

    // Then
    assertThat(db.isInTransaction(), is(false));
  }

  @Test
  public void commitClosesNestedTransaction() {

    // Given
    Database db = new Database();
    db.begin();
    db.begin();

    // When
    db.commit();

    // Then
    assertThat(db.isInTransaction(), is(false));
  }

  @Test
  public void commitKeepsTransactionChanges() {

    // Given
    Database db = new Database();
    db.begin();
    db.set("a", "10");

    // When
    db.commit();

    // Then
    assertThat(db.get("a"), is("10"));
  }

  @Test
  public void commitKeepsNestedTransactionChanges() {

    // Given
    Database db = new Database();
    db.begin();
    db.set("a", "10");
    db.begin();
    db.set("a", "20");

    // When
    db.commit();

    // Then
    assertThat(db.get("a"), is("20"));
  }

  @Test(expected = IllegalStateException.class)
  public void rollbackNoTransaction() {
    Database db = new Database();
    db.rollback();
  }

  @Test
  public void rollbackClosesOnlyTransaction() {

    // Given
    Database db = new Database();
    db.begin();

    // When
    db.rollback();

    // Then
    assertThat(db.isInTransaction(), is(false));
  }

  @Test
  public void rollbackClosesTopTransaction() {

    // Given
    Database db = new Database();
    db.begin();
    db.begin();

    // When
    db.rollback();

    // Then
    assertThat(db.isInTransaction(), is(true));
  }

  @Test
  public void rollbackDiscardsInsert() {

    // Given
    Database db = new Database();
    db.begin();
    db.set("a", "10");

    // When
    db.rollback();

    // Then
    assertThat(db.get("a"), is(nullValue(String.class)));
  }

  @Test
  public void rollbackDiscardsAddValue() {

    // Given
    Database db = new Database();
    db.begin();
    db.set("a", "10");

    // When
    db.rollback();

    // Then
    assertThat(db.numEqualTo("10"), is(0));
  }

  @Test
  public void rollbackDiscardsIncrement() {

    // Given
    Map<String, String> entries = new HashMap<>();
    entries.put("a", "10");
    entries.put("b", "10");
    entries.put("c", "20");
    entries.put("d", "10");

    Database db = new Database(entries);
    db.begin();
    db.set("e", "10");

    // When
    db.rollback();

    // Then
    assertThat(db.numEqualTo("10"), is(3));
  }

  @Test
  public void rollbackDiscardsDelete() {

    // Given
    Database db = new Database(Collections.singletonMap("a", "10"));
    db.begin();
    db.unset("a");

    // When
    db.rollback();

    // Then
    assertThat(db.get("a"), is("10"));
  }

  @Test
  public void rollbackDiscardsRemoveValue() {

    // Given
    Database db = new Database(Collections.singletonMap("a", "10"));
    db.begin();
    db.unset("a");

    // When
    db.rollback();

    // Then
    assertThat(db.numEqualTo("10"), is(1));
  }

  @Test
  public void rollbackDiscardsDecrement() {

    // Given
    Map<String, String> entries = new HashMap<>();
    entries.put("a", "10");
    entries.put("b", "10");
    entries.put("c", "20");
    entries.put("d", "10");

    Database db = new Database(entries);
    db.begin();
    db.unset("a");

    // When
    db.rollback();

    // Then
    assertThat(db.numEqualTo("10"), is(3));
  }

  @Test
  public void rollbackIgnoresNoOpDelete() {

    // Given
    Database db = new Database();
    db.begin();
    db.unset("a");

    // When
    db.rollback();

    // Then
    assertThat(db.get("a"), is(nullValue(String.class)));
    assertThat(db.numEqualTo(null), is(0));
  }

  @Test
  public void rollbackDiscardsUpdate() {

    // Given
    Database db = new Database(Collections.singletonMap("a", "10"));
    db.begin();
    db.set("a", "20");

    // When
    db.rollback();

    // Then
    assertThat(db.get("a"), is("10"));
  }

  @Test
  public void rollbackDiscardsReplaceValue() {

    // Given
    Database db = new Database(Collections.singletonMap("a", "10"));
    db.begin();
    db.set("a", "20");

    // When
    db.rollback();

    // Then
    assertThat(db.numEqualTo("10"), is(1));
    assertThat(db.numEqualTo("20"), is(0));
  }

  @Test
  public void rollbackDiscardsMultipleUpdates() {

    // Given
    Map<String, String> entries = new HashMap<>();
    entries.put("a", "10");
    entries.put("b", "10");
    entries.put("c", "20");
    entries.put("d", "10");

    Database db = new Database(entries);
    db.begin();
    db.set("a", "20");
    db.set("a", "30");
    db.unset("a");

    // When
    db.rollback();

    // Then
    assertThat(db.get("a"), is("10"));
    assertThat(db.numEqualTo("10"), is(3));
    assertThat(db.numEqualTo("20"), is(1));
    assertThat(db.numEqualTo("30"), is(0));
  }

  @Test
  public void rollbackDiscardsMultipleWrites() {

    // Given
    Map<String, String> entries = new HashMap<>();
    entries.put("a", "10");
    entries.put("b", "10");
    entries.put("c", "20");
    entries.put("d", "10");

    Database db = new Database(entries);
    db.begin();
    db.set("e", "10");
    db.set("e", "20");
    db.set("e", "30");

    // When
    db.rollback();

    // Then
    assertThat(db.get("e"), is(nullValue(String.class)));
    assertThat(db.numEqualTo("10"), is(3));
    assertThat(db.numEqualTo("20"), is(1));
    assertThat(db.numEqualTo("30"), is(0));
  }

  @Test
  public void rollbackDiscardsMultipleIndependentWrites() {

    // Given
    Map<String, String> entries = new HashMap<>();
    entries.put("a", "10");
    entries.put("b", "10");
    entries.put("c", "20");
    entries.put("d", "10");

    Database db = new Database(entries);
    db.begin();
    db.set("e", "10");
    db.set("a", "20");
    db.set("e", "20");
    db.set("a", "30");
    db.set("e", "30");
    db.unset("a");

    // When
    db.rollback();

    // Then
    assertThat(db.get("a"), is("10"));
    assertThat(db.get("e"), is(nullValue(String.class)));
    assertThat(db.numEqualTo("10"), is(3));
    assertThat(db.numEqualTo("20"), is(1));
    assertThat(db.numEqualTo("30"), is(0));
  }

  @Test
  public void nestedRollbackDiscardsWritesSinceLastCheckpoint() {

    // Given
    Map<String, String> entries = new HashMap<>();
    entries.put("a", "10");
    entries.put("b", "10");
    entries.put("c", "20");
    entries.put("d", "10");

    Database db = new Database(entries);
    db.begin();
    db.set("e", "10");
    db.set("a", "20");
    db.set("e", "20");
    db.begin();
    db.set("a", "30");
    db.set("e", "30");
    db.unset("a");

    // When
    db.rollback();

    // Then
    assertThat(db.get("a"), is("20"));
    assertThat(db.get("e"), is("20"));
    assertThat(db.numEqualTo("10"), is(2));
    assertThat(db.numEqualTo("20"), is(3));
    assertThat(db.numEqualTo("30"), is(0));
  }
}
