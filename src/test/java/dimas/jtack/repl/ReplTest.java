package dimas.jtack.repl;

import dimas.jtack.Database;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class ReplTest {

  private Database db;

  @Before
  public void setUp() throws Exception {
    db = new Database();
  }

  @Test
  public void scrubLines() {

    // Given
    Stream<String> lines =
        Arrays.asList(
            "  GET a",
            "   ",
            " \t  ",
            "SET a 10   ",
            "\t",
            "",
            "  \t UNSET a\t").stream();

    // When
    List<String> result = Repl.scrubLines(lines).collect(Collectors.toList());

    // Then
    assertThat(result, is(Arrays.asList("GET a", "SET a 10", "UNSET a")));
  }

  @Test
  public void parseSpacing() {

    // Given
    Repl repl = new Repl(db, Collections.<String>emptyList().stream());
    String line = "SET \t a\t10 c     meep\t\ttweet";

    // When
    Command result = repl.parse(line);

    // Then
    assertThat(
        result,
        is(new Command(Operation.SET, db, "a", "10", "c", "meep", "tweet")));

  }

  @Test
  public void parseLines() {
    // Given
    Stream<String> lines =
        Arrays.asList(
            "SET a",
            "get a 10",
            "uNsET a",
            "garply boop",
            "End",
            "Werple terp").stream();

    Repl repl = new Repl(db, lines);

    // When
    List<Command> commands =
        repl.parseLines(lines)
            .collect(Collectors.toList());

    // Then
    assertThat(
        commands,
        is(Arrays.asList(
            new Command(Operation.SET, db, "a"),
            new Command(Operation.GET, db, "a", "10"),
            new Command(Operation.UNSET, db, "a"),
            new Command(Operation.UNKNOWN, db, "boop"))));
  }

  @Test
  public void output() {

    // Given
    Stream<String> commands =
        Arrays.asList(
            "woops",
            "SET a",
            "GET a 10",
            "COMMIT",
            "ROLLBACK",
            "GET a",
            "BEGIN",
            "SET a 10",
            "GET a",
            "BEGIN",
            "UNSET a",
            "ROLLBACK",
            "NUMEQUALTO 10",
            "COMMIT",
            "GET a"
        ).stream();

    Repl repl = new Repl(db, commands);

    // When
    List<String> output = repl.run().collect(Collectors.toList());

    // Then
    assertThat(
        output,
        is(Arrays.asList(
            "UNKNOWN OPERATION",
            "WRONG ARGUMENTS - EXPECTED: 2, ACTUAL: 1",
            "WRONG ARGUMENTS - EXPECTED: 1, ACTUAL: 2",
            "NO TRANSACTION",
            "NO TRANSACTION",
            "NULL",
            "10",
            "1",
            "10")));
  }
}
