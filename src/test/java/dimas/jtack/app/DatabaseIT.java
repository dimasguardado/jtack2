package dimas.jtack.app;

import org.junit.Test;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class DatabaseIT {

  @Test
  public void basicData() throws IOException {
    testScript("basic-data");
  }

  @Test
  public void countData() throws IOException {
    testScript("count-data");
  }

  @Test
  public void rollbackTransaction() throws IOException {
    testScript("rollback-tx");
  }

  @Test
  public void commitTransaction() throws IOException {
    testScript("commit-tx");
  }

  @Test
  public void mixedTransaction() throws IOException {
    testScript("mixed-tx");
  }

  @Test
  public void countTransaction() throws IOException {
    testScript("count-tx");
  }

  private void testScript(String file) throws IOException {

    InputStream script = getScript(file);
    ByteArrayOutputStream out = new ByteArrayOutputStream();

    App app = new App(script, new PrintStream(out));

    app.run();

    String result = new String(out.toByteArray());
    String expected = getExpectedOutput(file);

    assertThat(result, is(expected));
  }

  private String getExpectedOutput(String file) throws IOException {
    return new String(
        Files.readAllBytes(
            Paths.get("src/test/resources/" + file + "-out.txt")));
  }

  private InputStream getScript(String file) throws IOException {
    return Files.newInputStream(
        Paths.get("src/test/resources/" + file + "-in.txt"));
  }
}
