package dimas.jtack;

interface Undo {
  void apply();
}
