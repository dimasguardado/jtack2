package dimas.jtack;

import java.util.HashMap;
import java.util.Map;

final class Storage {

  private final Map<String, String> entries;
  private final MultiSet values = new MultiSet();

  Storage(Map<String, String> entries) {
    this.entries = new HashMap<>(entries);
    this.entries.values().forEach(values::add);
  }

  String get(String name) {
    return entries.get(name);
  }

  int numEqualTo(String name) {
    return values.count(name);
  }

  String set(String name, String value) {
    String old = entries.put(name, value);
    values.remove(old);
    values.add(value);
    return old;
  }

  String unset(String name) {
    String old = entries.remove(name);
    values.remove(old);
    return old;
  }
}
