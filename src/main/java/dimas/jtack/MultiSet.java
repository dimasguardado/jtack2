package dimas.jtack;

import java.util.HashMap;
import java.util.Map;

final class MultiSet {

  private final Map<String, Integer> values = new HashMap<>();

  int count(String name) {
    return values.getOrDefault(name, 0);
  }

  void add(String value) {
    values.merge(value, 1, Integer::sum);
  }

  void remove(String value) {
    values.computeIfPresent(value, (val, count) -> decrementOrRemove(count));
  }

  private Integer decrementOrRemove(int count) {
    return (count <= 1) ? null : (count - 1);
  }
}
