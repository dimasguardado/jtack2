package dimas.jtack;

import java.util.ArrayDeque;
import java.util.Deque;

final class TransactionManager {

  private final Storage db;
  private final Deque<Transaction> transactions = new ArrayDeque<>();

  TransactionManager(Storage db) {
    this.db = db;
  }

  void logSet(String name, String old) {
    if (isInTransaction()) {
      transactions.peek().logSet(name, old);
    }
  }

  void logUnset(String name, String old) {
    if (isInTransaction()) {
      transactions.peek().logUnset(name, old);
    }
  }

  boolean isInTransaction() {
    return !transactions.isEmpty();
  }

  void begin() {
    transactions.push(new Transaction(db));
  }

  void commit() {
    checkTransactionPresent();
    transactions.clear();
  }

  void rollback() {
    checkTransactionPresent();
    Transaction transaction = transactions.pop();
    transaction.rollback();
  }

  private void checkTransactionPresent() {
    if (!isInTransaction()) {
      throw new IllegalStateException("No Transaction");
    }
  }
}
