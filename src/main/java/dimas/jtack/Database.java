package dimas.jtack;

import java.util.HashMap;
import java.util.Map;

public class Database {

  private final Storage storage;
  private final TransactionManager transactions;

  Database(Map<String, String> entries) {
    storage = new Storage(entries);
    transactions = new TransactionManager(storage);
  }

  public Database() {
    this(new HashMap<>());
  }

  public String get(String name) {
    return storage.get(name);
  }

  public int numEqualTo(String name) {
    return storage.numEqualTo(name);
  }

  public void set(String name, String value) {
    String old = storage.set(name, value);
    transactions.logSet(name, old);
  }

  public void unset(String name) {
    String old = storage.unset(name);
    transactions.logUnset(name, old);
  }

  public boolean isInTransaction() {
    return transactions.isInTransaction();
  }

  public void begin() {
    transactions.begin();
  }

  public void commit() {
    transactions.commit();
  }

  public void rollback() {
    transactions.rollback();
  }
}
