package dimas.jtack;

import java.util.ArrayDeque;
import java.util.Deque;

final class Transaction {

  private final Storage db;
  private final Deque<Undo> log = new ArrayDeque<>();

  Transaction(Storage db) {
    this.db = db;
  }

  void logSet(String name, String old) {

    Undo undo =
        (old == null)
            ? new Unset(db, name)
            : new Set(db, name, old);

    log.push(undo);
  }

  void logUnset(String name, String old) {
    if (old != null) {
      log.push(new Set(db, name, old));
    }
  }

  void rollback() {
    while (!log.isEmpty()) {
      Undo undo = log.pop();
      undo.apply();
    }
  }
}
