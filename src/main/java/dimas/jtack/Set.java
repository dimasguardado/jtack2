package dimas.jtack;

final class Set implements Undo {

  private final Storage db;
  private final String name;
  private final String value;

  Set(Storage db, String name, String value) {
    this.db = db;
    this.name = name;
    this.value = value;
  }

  @Override
  public void apply() {
    db.set(name, value);
  }
}
