package dimas.jtack.repl;

import dimas.jtack.Database;

import java.util.Arrays;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class Repl {

  private final Database db;
  private final Stream<String> lines;

  public Repl(Database db, Stream<String> lines) {
    this.db = db;
    this.lines = lines;
  }

  static Stream<String> scrubLines(Stream<String> lines) {
    return lines.map(String::trim).filter(l -> !l.isEmpty());
  }

  Command parse(String line) {
    String[] command = line.split("\\s+");
    Operation op = Operation.parse(command[0]);
    String[] args = Arrays.copyOfRange(command, 1, command.length);
    return new Command(op, db, args);
  }

  Stream<Command> parseLines(Stream<String> lines) {
    return takeUntilEnd(lines.map(this::parse));
  }

  private static Stream<Command> takeUntilEnd(Stream<Command> commands) {
    Iterable<Command> iterable = () -> new Taker(commands.iterator());
    return StreamSupport.stream(iterable.spliterator(), false);
  }

  public Stream<String> run() {
    Stream<Command> commands = parseLines(scrubLines(lines));
    return commands.map(Command::eval).filter(s -> !s.isEmpty());
  }
}
