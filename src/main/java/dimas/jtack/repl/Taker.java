package dimas.jtack.repl;

import java.util.Iterator;
import java.util.NoSuchElementException;

final class Taker implements Iterator<Command> {

  private final Iterator<Command> iterator;
  private Command current;

  Taker(Iterator<Command> iterator) {
    this.iterator = iterator;
  }

  @Override
  public boolean hasNext() {
    if (current == null) {
      if (!iterator.hasNext()) {
        return false;
      }
      current = iterator.next();
    }

    return !current.isEnd();
  }

  @Override
  public Command next() {
    if (!hasNext()) {
      throw new NoSuchElementException();
    }

    Command next = current;
    current = null;
    return next;
  }
}
