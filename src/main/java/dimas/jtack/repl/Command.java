package dimas.jtack.repl;

import dimas.jtack.Database;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

final class Command {

  private final Operation operation;
  private final Database db;
  private final List<String> args;

  Command(Operation operation, Database db, String... args) {
    this.operation = operation;
    this.db = db;
    this.args = Arrays.asList(args);
  }

  boolean isEnd() {
    return operation.equals(Operation.END);
  }

  String eval() {
    return operation.apply(db, args);
  }

  @Override
  public boolean equals(Object o) {

    if (this == o) {
      return true;
    }

    if (o == null) {
      return false;
    }

    if (!getClass().equals(o.getClass())) {
      return false;
    }

    Command command = (Command) o;

    return operation.equals(command.operation)
        && db.equals(command.db)
        && args.equals(command.args);
  }

  @Override
  public int hashCode() {
    return Objects.hash(operation, db, args);
  }

  @Override
  public String toString() {
    return String.format(
        "%1$s %2$s | %3$s",
        operation,
        args.stream().collect(Collectors.joining(" ")),
        db);
  }
}
