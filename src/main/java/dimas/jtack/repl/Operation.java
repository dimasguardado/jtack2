package dimas.jtack.repl;

import dimas.jtack.Database;

import java.util.List;

enum Operation {

  SET(2) {
    @Override
    String call(Database db, List<String> args) {
      db.set(args.get(0), args.get(1));
      return "";
    }
  },

  UNSET(1) {
    @Override
    String call(Database db, List<String> args) {
      db.unset(args.get(0));
      return "";
    }
  },

  GET(1) {
    @Override
    String call(Database db, List<String> args) {
      String result = db.get(args.get(0));
      return (result == null) ? "NULL" : result;
    }
  },

  NUMEQUALTO(1) {
    @Override
    String call(Database db, List<String> args) {
      return String.valueOf(db.numEqualTo(args.get(0)));
    }
  },

  BEGIN {
    @Override
    String call(Database db, List<String> args) {
      db.begin();
      return "";
    }
  },

  COMMIT {
    @Override
    String call(Database db, List<String> args) {

      if (!db.isInTransaction()) {
        return "NO TRANSACTION";
      }

      db.commit();

      return "";
    }
  },

  ROLLBACK {
    @Override
    String call(Database db, List<String> args) {

      if (!db.isInTransaction()) {
        return "NO TRANSACTION";
      }

      db.rollback();

      return "";
    }
  },

  END(-1) {
    @Override
    String call(Database db, List<String> args) {
      throw new UnsupportedOperationException();
    }
  },

  UNKNOWN(-1) {
    @Override
    String call(Database db, List<String> args) {
      return "UNKNOWN OPERATION";
    }
  };

  private final int expectedArgs;

  Operation(int expectedArgs) {
    this.expectedArgs = expectedArgs;
  }

  Operation() {
    this(0);
  }

  static Operation parse(String op) {
    try {
      return valueOf(op.toUpperCase());
    } catch (IllegalArgumentException ignored) {
      return UNKNOWN;
    }
  }

  String apply(Database db, List<String> args) {
    return areValid(args)
        ? call(db, args)
        : reject(args);
  }

  private String reject(List<String> args) {
    return String.format(
        "WRONG ARGUMENTS - EXPECTED: %1$s, ACTUAL: %2$s",
        expectedArgs, args.size());
  }

  private boolean areValid(List<String> args) {
    return (expectedArgs < 0 || expectedArgs == args.size());
  }

  abstract String call(Database db, List<String> args);
}
