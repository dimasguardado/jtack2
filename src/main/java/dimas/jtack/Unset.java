package dimas.jtack;

final class Unset implements Undo {

  private final Storage db;
  private final String name;

  Unset(Storage db, String name) {
    this.db = db;
    this.name = name;
  }

  @Override
  public void apply() {
    db.unset(name);
  }
}
