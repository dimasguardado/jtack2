package dimas.jtack.app;

import dimas.jtack.Database;
import dimas.jtack.repl.Repl;

import java.io.*;

public class App {

  private final BufferedReader in;
  private final PrintStream out;

  App(InputStream in, PrintStream out) {
    this.in = new BufferedReader(new InputStreamReader(in));
    this.out = out;
  }

  void run() {
    new Repl(new Database(), in.lines())
        .run()
        .forEachOrdered(out::println);
  }

  public static void main(String[] args) {
    System.out.println("Welcome to Tack DB! Please enter a command.");
    new App(System.in, System.out).run();
    System.out.println("Bye for now!");
  }
}
