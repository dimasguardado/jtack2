# jTackDB

jTackDB is a small, single-user, non-durable, transactional key-value database
written in Java. It was written as a coding exercise and is not intended
by any means for production usage. The design of the API is part of
[Thumbtack, Inc.'s Software Engineer Challenges](http://www.thumbtack.com/challenges/software-engineer).

## Installation

The project can be cloned from git@gitlab.com:dimasguardado/jtack2.git
and built with Maven

```bash
$ git clone git@gitlab.com:dimasguardado/jtack2.git
$ cd jtack2
$ mvn verify
```

The build requires the JAVA_HOME environment variable to be set to a
Java 8 JDK

## Usage

Once a jar is built, it can be invoked in the command-line, like so:

    $ java -jar target/jtack2.jar

## License

Copyright © 2014 Dimas Guardado, Jr. All rights reserved.
